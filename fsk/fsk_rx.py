#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Fsk Rx
# GNU Radio version: 3.8.1.0

from gnuradio import blocks
from gnuradio import digital
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import iio


class fsk_rx(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Fsk Rx")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000
        self.freq_rx_1 = freq_rx_1 = 2310000000
        self.code2 = code2 = '11011010110111011000110011110101100010010011110111'

        ##################################################
        # Blocks
        ##################################################
        self.iio_fmcomms2_source_0 = iio.fmcomms2_source_f32c('local:', freq_rx_1, 2084000, 2000000, True, False, 0x8000, True, True, True, 'manual', 64.0, 'manual', 64.0, 'A_BALANCED', '', True)
        self.digital_gfsk_demod_0 = digital.gfsk_demod(
            samples_per_symbol=8,
            sensitivity=0.300,
            gain_mu=0.175,
            mu=0.5,
            omega_relative_limit=0.005,
            freq_error=0.0,
            verbose=False,
            log=False)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, 'recibido.txt', False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.digital_gfsk_demod_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.iio_fmcomms2_source_0, 0), (self.digital_gfsk_demod_0, 0))


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_freq_rx_1(self):
        return self.freq_rx_1

    def set_freq_rx_1(self, freq_rx_1):
        self.freq_rx_1 = freq_rx_1
        self.iio_fmcomms2_source_0.set_params(self.freq_rx_1, 2084000, 2000000, True, True, True, 'manual', 64.0, 'manual', 64.0, 'A_BALANCED', '', True)

    def get_code2(self):
        return self.code2

    def set_code2(self, code2):
        self.code2 = code2





def main(top_block_cls=fsk_rx, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print("Error: failed to enable real-time scheduling.")
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
