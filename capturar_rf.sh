#!/bin/sh

DEVICE=local:
FREQ=1575420000
FS=2100000
BW=2050000
GAIN_MODE=manual
GAIN=62
PORT=A_BALANCED
NSAMPLES=210000000

FILE=/tmp/samples.dat
 
iio_attr -u ${DEVICE} -c ad9361-phy RX_LO frequency ${FREQ}
iio_attr -u ${DEVICE} -c ad9361-phy voltage0 gain_control_mode ${GAIN_MODE}
iio_attr -u ${DEVICE} -i -c ad9361-phy voltage0 hardwaregain ${GAIN}
iio_attr -u ${DEVICE} -i -c ad9361-phy voltage0 rf_bandwidth ${BW}
iio_attr -u ${DEVICE} -i -c ad9361-phy voltage0 rf_port_select ${PORT}
iio_attr -u ${DEVICE} -i -c ad9361-phy voltage0 sampling_frequency ${FS}
echo 39999850 > /sys/bus/iio/devices/iio\:device2/xo_correction

echo "capturando..."
iio_readdev -u ${DEVICE} -s ${NSAMPLES} cf-ad9361-lpc > ${FILE}
